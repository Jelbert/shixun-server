package com.easy.lab.shixun.common.enums;

import com.google.common.base.Enums;

/**
 * 消息类型
 * 配合注解{@see com.easy.lab.shixun.common.annotation.SxEventProcessor}使用
 *
 * @Author: https://gitee.com/wesleyOne
 * @Date: 12.07 2019
 */
public enum MessageTypeEnums {

    /**
     * 签到
     */
    SIGN_START("签到开始"),
    SIGN_PROCESS("签到处理中"),
    SIGN_END("签到结束"),
    ;

    String name;

    MessageTypeEnums(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static MessageTypeEnums getIfPresent(String name) {
        return Enums.getIfPresent(MessageTypeEnums.class, name).orNull();
    }

}
