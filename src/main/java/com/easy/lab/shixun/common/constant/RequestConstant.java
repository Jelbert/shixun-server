package com.easy.lab.shixun.common.constant;

/**
 * @Author: https://gitee.com/wesleyOne
 * @Date: 12.08 2019
 */
public interface RequestConstant {
    String TOKEN = "token";
    String GROUP_UNIQUE_ID = "guid";

    String INDEX_PAGE = "home";
}
