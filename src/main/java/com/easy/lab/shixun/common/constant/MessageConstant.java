package com.easy.lab.shixun.common.constant;

/**
 * 常量
 * @Author: https://gitee.com/wesleyOne
 * @Date: 12.07 2019
 */
public interface MessageConstant {

    String STOMP_ENDPOINT = "/stomp";
    String BROKER_QUEUE = "/queue";
    String BROKER_TOPIC = "/topic";
    String BROKER_QUEUE_PREFIX = "/user";

}
