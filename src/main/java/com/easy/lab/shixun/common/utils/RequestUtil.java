package com.easy.lab.shixun.common.utils;

import com.easy.lab.shixun.common.constant.RequestConstant;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author: https://gitee.com/wesleyOne
 * @Date: 12.09 2019
 */
public class RequestUtil {

    public static String getCookievalue(HttpServletRequest request) {
        Cookie[] cookies =  request.getCookies();
        if(cookies != null){
            for(Cookie cookie : cookies){
                if(cookie.getName().equals(RequestConstant.TOKEN)){
                    return cookie.getValue();
                }
            }
        }
        return null;
    }

    public static void setCookieValue(String key, String value, HttpServletResponse response) {
        Cookie cookie=new Cookie(key,value);
        response.addCookie(cookie);
    }

    public static void clearCookie(String key, HttpServletResponse response) {
        Cookie cookie=new Cookie(key,"");
        cookie.setMaxAge(1);
        response.addCookie(cookie);
    }
}
