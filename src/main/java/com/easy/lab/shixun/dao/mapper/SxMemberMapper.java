package com.easy.lab.shixun.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.easy.lab.shixun.dao.dataobject.SxMember;

/**
 * @Author: https://gitee.com/wesleyOne
 * @Date: 12.01 2019
 */
public interface SxMemberMapper extends BaseMapper<SxMember> {
}
