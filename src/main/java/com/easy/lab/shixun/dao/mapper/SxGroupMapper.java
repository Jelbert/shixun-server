package com.easy.lab.shixun.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.easy.lab.shixun.dao.dataobject.SxGroup;

/**
 * @Author: https://gitee.com/wesleyOne
 * @Date: 12.01 2019
 */
public interface SxGroupMapper extends BaseMapper<SxGroup> {
}
