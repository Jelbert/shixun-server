package com.easy.lab.shixun.autoconfig;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @Author: https://gitee.com/wesleyOne
 * @Date: 12.15 2019
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .pathMapping("/")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.easy.lab.shixun.controller"))
                .paths(PathSelectors.any())
                .build().apiInfo(new ApiInfoBuilder()
                        .title("Shixun视讯 接口文档")
                        .description("开源的一站式多人会议(视频、语音、桌面分享)解决方案 https://gitee.com/wesleyOne/shixun-server")
                        .version("1.0")
                        .contact(new Contact("WesleyOne","https://gitee.com/wesleyOne","come0n@foxmail.com"))
                        .build());
    }

}
