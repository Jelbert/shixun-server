package com.easy.lab.shixun.controller;

import com.easy.lab.shixun.api.bo.request.GroupRequest;
import com.easy.lab.shixun.api.bo.response.GroupResponse;
import com.easy.lab.shixun.api.bo.response.TokenInfoResponse;
import com.easy.lab.shixun.api.service.GroupMemberService;
import com.easy.lab.shixun.api.service.LoginService;
import com.easy.lab.shixun.common.base.BaseError;
import com.easy.lab.shixun.common.base.BaseException;
import com.easy.lab.shixun.common.base.BaseResponse;
import com.easy.lab.shixun.common.utils.RequestUtil;
import com.easy.lab.shixun.api.bo.request.OpenviduRequest;
import com.easy.lab.shixun.api.bo.response.OpenviduResponse;
import com.easy.lab.shixun.service.manager.SimpleCoreManager;
import io.openvidu.java.client.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * Openvidu对接
 * @Author: https://gitee.com/wesleyOne
 * @Date: 12.15 2019
 */
@Api(tags = "Openvidu相关接口")
@Slf4j
@RestController
@RequestMapping("/ov")
public class OpenviduController {

    @Autowired
    private LoginService loginService;

    @Autowired
    private GroupMemberService groupMemberService;

    @Autowired
    private SimpleCoreManager simpleCoreManager;

    @Autowired
    private OpenVidu openVidu;

	@ApiOperation("初始化接口")
	@ResponseBody
	@RequestMapping(value = "/init", method = { RequestMethod.POST })
	public BaseResponse<OpenviduResponse> addUser(@RequestBody OpenviduRequest openviduRequest, HttpServletRequest request) {
		TokenInfoResponse tokenInfoResponse = checkUserLogged(request);
		if (Objects.isNull(tokenInfoResponse)) {
			return BaseResponse.error(BaseError.UNLOGIN);
		}
		OpenviduResponse openviduResponse = new OpenviduResponse();
		try {
			String groupUniqueId = openviduRequest.getGroupUniqueId();
			if (StringUtils.isBlank(groupUniqueId)) {
				return BaseResponse.error(BaseError.NORMAL_ERR);
			}
			GroupRequest groupRequest = new GroupRequest();
			groupRequest.setGroupUniqueId(groupUniqueId);
			GroupResponse groupMemberList = groupMemberService.getGroupInfo(groupRequest);
			if (Objects.isNull(groupMemberList)) {
				return BaseResponse.error(BaseError.NORMAL_ERR);
			}

			OpenViduRole role = tokenInfoResponse.getRole();
			if (Objects.isNull(role)) {
				role = OpenViduRole.PUBLISHER;
			}
			// 该用户连接到视频通话时要传递给其他用户的可选数据
			String serverData = tokenInfoResponse.makeServerData();
			// 通过serverData传递用户信息和指定角色
			TokenOptions tokenOptions = new TokenOptions.Builder().data(serverData).role(role).build();
			Session session = simpleCoreManager.getOpenviduSessions().get(groupUniqueId);
			if (session != null) {
				// token 已存在时
				System.out.println("群视频已存在" + groupUniqueId);
			} else {
				// 生成新的OpenVidu Session
				log.info("新建群视频: {}", groupUniqueId);
				// 增强配置（录制类型：手动还是一直；录制输出形式：每个视频独立还是聚合）
				// 强制会话id等于群id(可能后面有用)
				SessionProperties properties = new SessionProperties.Builder()
						.recordingMode(RecordingMode.MANUAL)
						.defaultOutputMode(Recording.OutputMode.INDIVIDUAL)
						.customSessionId(groupUniqueId)
						.build();
				session = this.openVidu.createSession(properties);
				simpleCoreManager.addOpenviduSession(groupUniqueId, session);
			}
			// 通过刚产生的令牌tokenOptions生成新的token(实质上是传递用户信息，并返回会议服里的唯一token)
			String openviduToken = session.generateToken(tokenOptions);
			simpleCoreManager.addOpenviduToken(tokenInfoResponse.getToken(), openviduToken);
			openviduResponse.setOpenViduToken(openviduToken);
		} catch (BaseException e1) {
			log.error("用户初始化Openvidu获取openViduToken失败",e1);
			return BaseResponse.error(e1.getCode(), e1.getMessage());
        } catch (Exception e) {
			log.error("用户初始化Openvidu获取openViduToken失败",e);
			return BaseResponse.error(BaseError.SYSTEM_ERR);
		}
		return BaseResponse.success(openviduResponse);
	}

	@ApiOperation("退出视频会话")
	@ResponseBody
	@RequestMapping(value = "/leave", method = RequestMethod.POST)
	public BaseResponse removeUser(HttpServletRequest request) {
		TokenInfoResponse tokenInfoResponse = checkUserLogged(request);
		if (Objects.isNull(tokenInfoResponse)) {
			return BaseResponse.OK;
		}
		log.info("{} 离开了视频会议" , tokenInfoResponse.getMemberNickname());
		simpleCoreManager.removeOpenviduToken(tokenInfoResponse.getToken());
		return BaseResponse.OK;
	}

	private TokenInfoResponse checkUserLogged(HttpServletRequest request) {
        String token = RequestUtil.getCookievalue(request);
        if ( StringUtils.isNotBlank(token) ) {
            TokenInfoResponse tokenInfoResponse = loginService.checkLogin(token);
            if (Objects.nonNull(tokenInfoResponse) && tokenInfoResponse.getIsLogin()) {
                return tokenInfoResponse;
            }
        }
        return null;
	}

	@ApiOperation("录制接口")
	@ResponseBody
	@RequestMapping(value = "/record", method = RequestMethod.POST)
	public BaseResponse record(@RequestBody OpenviduRequest openviduRequest, HttpServletRequest request) {
		TokenInfoResponse tokenInfoResponse = checkUserLogged(request);
		if (Objects.isNull(tokenInfoResponse) || Objects.isNull(tokenInfoResponse.getGroupUniqueId())) {
			return BaseResponse.error(BaseError.NORMAL_ERR);
		}

		if (openviduRequest.getState()) {
			log.info("{} 启动录制 {}" , tokenInfoResponse.getMemberNickname(), tokenInfoResponse.getGroupUniqueId());
			String recordName = tokenInfoResponse.getGroupUniqueId().concat("_").concat(tokenInfoResponse.getMemberUniqueId())
					.concat("_").concat(RandomStringUtils.randomAlphanumeric(6));
			RecordingProperties properties = new RecordingProperties.Builder()
					.outputMode(Recording.OutputMode.INDIVIDUAL)
					.name(recordName)
					.build();
			Session session = simpleCoreManager.getOpenviduSessions().get(tokenInfoResponse.getGroupUniqueId());
			if (Objects.nonNull(session)) {
				try {
					Recording recording = openVidu.startRecording(session.getSessionId(), properties);
					simpleCoreManager.addOpenviduRecord(tokenInfoResponse.getGroupUniqueId(), recording.getId());
				} catch (OpenViduJavaClientException | OpenViduHttpException e) {
					e.printStackTrace();
					return BaseResponse.error(BaseError.SYSTEM_ERR);
				}
			}
		} else {
			log.info("{} 停止录制 {}" , tokenInfoResponse.getMemberNickname(), tokenInfoResponse.getGroupUniqueId());
			String recordingId = simpleCoreManager.getOpenviduRecordings().get(tokenInfoResponse.getGroupUniqueId());
			if (Objects.nonNull(recordingId)) {
				try {
					openVidu.stopRecording(recordingId);
				} catch (OpenViduJavaClientException | OpenViduHttpException e) {
					e.printStackTrace();
					return BaseResponse.error(BaseError.SYSTEM_ERR);
				}
			}
		}
		return BaseResponse.OK;
	}

}
