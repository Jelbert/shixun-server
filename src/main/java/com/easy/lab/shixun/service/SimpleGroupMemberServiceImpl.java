package com.easy.lab.shixun.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.easy.lab.shixun.api.bo.request.GroupRequest;
import com.easy.lab.shixun.api.bo.response.GroupResponse;
import com.easy.lab.shixun.api.bo.response.TokenInfoResponse;
import com.easy.lab.shixun.api.bo.response.MemberResponse;
import com.easy.lab.shixun.api.service.GroupMemberService;
import com.easy.lab.shixun.api.service.LoginService;
import com.easy.lab.shixun.common.base.BaseException;
import com.easy.lab.shixun.dao.dataobject.SxGroup;
import com.easy.lab.shixun.dao.mapper.SxGroupMapper;
import com.easy.lab.shixun.service.manager.SimpleCoreManager;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketSession;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 群成员接口简单实现
 * @Author: https://gitee.com/wesleyOne
 * @Date: 12.07 2019
 */
@Primary
@Service
public class SimpleGroupMemberServiceImpl implements GroupMemberService {

    @Autowired
    private SxGroupMapper sxGroupMapper;

    @Autowired
    private LoginService loginService;

    @Autowired
    private SimpleCoreManager simpleCoreManager;

    @Override
    public List<GroupResponse> getGroupList(GroupRequest groupRequest) {
        List<GroupResponse> resultBOList = new ArrayList<>();
        List<SxGroup> sxGroups = sxGroupMapper.selectList(new QueryWrapper<SxGroup>().lambda());
        if (CollectionUtils.isNotEmpty(sxGroups)) {
            sxGroups.forEach(g -> {
                GroupResponse groupResponse = new GroupResponse();
                groupResponse.setGroupName(g.getGroupName());
                groupResponse.setGroupUniqueId(g.getGroupUniqueId());
                resultBOList.add(groupResponse);
            });
        }
        return resultBOList;
    }

    @Override
    public GroupResponse getGroupInfo(GroupRequest groupRequest) {
        if (Objects.isNull(groupRequest)
                || Objects.isNull(groupRequest.getGroupUniqueId()) ) {
            return null;
        }
        SxGroup sxGroup = sxGroupMapper.selectOne(new QueryWrapper<SxGroup>().lambda()
                .eq(SxGroup::getGroupUniqueId, groupRequest.getGroupUniqueId()));
        if (Objects.isNull(sxGroup)) {
            return null;
        }
        GroupResponse groupResponse = new GroupResponse();
        groupResponse.setGroupName(sxGroup.getGroupName());
        groupResponse.setGroupUniqueId(sxGroup.getGroupUniqueId());
        return groupResponse;
    }

    private List<MemberResponse> changeMemberTokenToMemberInfo(List<String> memberTokenList) {
        List<TokenInfoResponse> infosByTokens = loginService.getInfosByTokens(memberTokenList);
        List<MemberResponse> memberResponses = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(infosByTokens)) {
            infosByTokens.forEach(info -> {
                MemberResponse memberResponse = new MemberResponse();
                memberResponse.setMemberNickname(info.getMemberNickname());
                memberResponse.setMemberUniqueId(info.getMemberUniqueId());
                memberResponses.add(memberResponse);
            });
        }
        return memberResponses;
    }

    @Override
    public void relateTokenWithGroup(String token, String groupUniqueId) throws BaseException {
        Objects.requireNonNull(token,"TOKEN不允许空");
        Objects.requireNonNull(groupUniqueId,"群id不允许空");
        simpleCoreManager.addGroupMember(groupUniqueId, token);
    }

    @Override
    public void relateTokenWithWebSocketSession(String token, WebSocketSession webSocketSession) throws BaseException {
        Objects.requireNonNull(token,"ws令牌不允许空");
        Objects.requireNonNull(webSocketSession,"ws会话不允许空");
        simpleCoreManager.addWebSocketSession(token, webSocketSession);
    }

    @Override
    public void removeTokenWsSession(String token) {
        if (Objects.isNull(token)) {
            return;
        }
        simpleCoreManager.removeWebSocketSession(token);
    }

    @Override
    public List<String> getMembers(String groupUniqueId) {
        return simpleCoreManager.getReadOnlyGroupRefMember().getOrDefault(groupUniqueId, new ArrayList<>());
    }

    @Override
    public boolean containGroup(String groupUniqueId) {
        return simpleCoreManager.getReadOnlyGroupRefMember().containsKey(groupUniqueId);
    }

    @Override
    public boolean containMember(String token) {
        return simpleCoreManager.getReadOnlyTokenInfo().containsKey(token);
    }


}
