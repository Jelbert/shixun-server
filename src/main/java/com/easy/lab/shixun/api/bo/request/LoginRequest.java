package com.easy.lab.shixun.api.bo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 登陆参数
 * @Author: https://gitee.com/wesleyOne
 * @Date: 12.07 2019
 */
@ApiModel
@Data
public class LoginRequest {
    @ApiModelProperty(value = "帐号")
    String username;
    @ApiModelProperty(value = "密码")
    String password;
    @ApiModelProperty(value = "会话令牌", hidden=true)
    String token;
}
