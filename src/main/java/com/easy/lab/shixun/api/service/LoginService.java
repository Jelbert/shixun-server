package com.easy.lab.shixun.api.service;

import com.easy.lab.shixun.api.bo.request.LoginRequest;
import com.easy.lab.shixun.api.bo.response.TokenInfoResponse;

import java.util.List;

/**
 * 登陆登出相关
 * @Author: https://gitee.com/wesleyOne
 * @Date: 12.07 2019
 */
public interface LoginService {

    /**
     * 登录令牌
     * @param loginParam    登陆参数
     * @return  登录信息
     */
    TokenInfoResponse login(LoginRequest loginParam);

    /**
     * 退出
     * @param loginParam    退出参数
     * @return  登录信息
     */
    TokenInfoResponse logout(LoginRequest loginParam);

    /**
     * 检查登录令牌
     * @param token    登录令牌
     * @return  登录信息
     */
    TokenInfoResponse checkLogin(String token);

    /**
     * 设置token
     * @param token             登录令牌
     * @param tokenInfoResponse     登录结果
     */
    void setLoginResult(String token, TokenInfoResponse tokenInfoResponse);

    /**
     * 根据token查找所有用户信息
     * @param tokens    tokens
     * @return          登入信息
     */
    List<TokenInfoResponse> getInfosByTokens(List<String> tokens);
}
