package com.easy.lab.shixun.api.bo.response;

import lombok.Data;

/**
 * 成员信息
 *
 * @Author: https://gitee.com/wesleyOne
 * @Date: 12.07 2019
 */
@Data
public class MemberResponse {

    /**
     * 成员唯一码
     */
    String memberUniqueId;

    /**
     * 成员昵称
     */
    String memberNickname;
}
