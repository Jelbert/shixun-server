package com.easy.lab.shixun.api.bo.request;

import com.easy.lab.shixun.api.bo.MessageBase;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 请求消息
 * @Author: https://gitee.com/wesleyOne
 * @Date: 12.07 2019
 */
@ApiModel
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Data
public class MessageRequest extends MessageBase {

    /**
     * 消息来源识别号
     */
    @ApiModelProperty(value = "消息来源识别号")
    private String sourcePrincipal;

    /**
     * 消息指定接受者识别号
     */
    @ApiModelProperty(value = "消息指定接受者识别号")
    private String toPrincipal;
}
