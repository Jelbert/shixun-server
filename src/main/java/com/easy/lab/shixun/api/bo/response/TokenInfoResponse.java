package com.easy.lab.shixun.api.bo.response;

import com.alibaba.fastjson.JSONObject;
import io.openvidu.java.client.OpenViduRole;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 登陆结果信息
 *
 * @Author: https://gitee.com/wesleyOne
 * @Date: 12.07 2019
 */
@ApiModel
@Data
public class TokenInfoResponse {
    @ApiModelProperty(value = "成员唯一编码")
    String memberUniqueId;
    @ApiModelProperty(value = "成员昵称")
    String memberNickname;
    @ApiModelProperty(value = "会话令牌", hidden = true)
    String token;
    @ApiModelProperty(value = "登录状态")
    Boolean isLogin;
    @ApiModelProperty(value = "视频角色")
    OpenViduRole role;
    @ApiModelProperty(value = "关联群唯一编码")
    String groupUniqueId;
    @ApiModelProperty(value = "视频会话令牌")
    String openviduToken;

    /**
     * 用于openvidu客户端之间共享的信息
     * @return
     */
    public String makeServerData() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("memberUniqueId",this.memberUniqueId);
        jsonObject.put("memberNickname",this.memberNickname);
        jsonObject.put("memberToken",this.token);
        return jsonObject.toJSONString();
    }
}
