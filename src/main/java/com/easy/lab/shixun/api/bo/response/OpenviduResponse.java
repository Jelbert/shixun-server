package com.easy.lab.shixun.api.bo.response;

import lombok.Data;

/**
 * @Author: https://gitee.com/wesleyOne
 * @Date: 12.12 2019
 */
@Data
public class OpenviduResponse {
    String openViduToken;
}
