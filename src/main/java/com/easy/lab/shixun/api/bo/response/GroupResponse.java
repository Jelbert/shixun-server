package com.easy.lab.shixun.api.bo.response;

import lombok.Data;

import java.util.List;

/**
 * 群信息
 * @Author: https://gitee.com/wesleyOne
 * @Date: 12.07 2019
 */
@Data
public class GroupResponse {
    /**
     * 群唯一码
     */
    String groupUniqueId;
    /**
     * 群名称
     */
    String groupName;
    /**
     * 成员信息
     */
    List<MemberResponse> memberResponseList;
}
