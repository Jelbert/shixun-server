> [OpenVidu官方授权中文翻译网站，点击查看更详细文档](http://openvidu_cn.gitee.io/openvidu_docs_cn/docs/)

> 感兴趣的同学可以加入钉钉群21919158，一起交流学习🎉。

> 也可以关注下gitee组织帐号[ :point_right: OpenVidu-KMS 兴趣圈 :point_left: ](https://gitee.com/OpenVidu_CN)
           
          
            ███████╗██╗  ██╗██╗██╗  ██╗██╗   ██╗███╗   ██╗
            ██╔════╝██║  ██║██║╚██╗██╔╝██║   ██║████╗  ██║
            ███████╗███████║██║ ╚███╔╝ ██║   ██║██╔██╗ ██║
            ╚════██║██╔══██║██║ ██╔██╗ ██║   ██║██║╚██╗██║
            ███████║██║  ██║██║██╔╝ ██╗╚██████╔╝██║ ╚████║
            ╚══════╝╚═╝  ╚═╝╚═╝╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═══╝
           
#### 介绍

网页视频会议开源解决方案

![分享桌面](https://images.gitee.com/uploads/images/2019/1215/202822_f2a2ffa8_1581722.png "截屏2019-12-15下午8.12.391.png")

![视频](https://images.gitee.com/uploads/images/2019/1215/202843_aa09970a_1581722.png "截屏2019-12-15下午8.12.521.png")

#### 软件架构
##### 视频服务：
* [OpenVidu官网链接](https://openvidu.io/) [OpenVidu的Github](https://github.com/OpenVidu) [OpenVidu的中文文档](http://openvidu_cn.gitee.io/openvidu_docs_cn/docs/home/)
> OpenVidu真的是非常好的项目，请为他star🌟

##### 后端服务：
* [Springboot](https://spring.io/projects/spring-boot/)
* [Mybatis-plus](https://mp.baomidou.com/)

##### 前端(正在邀请前端老司机扩展 @hertzz52 )：
* h5(后端调试用的，问题应该很多)

#### 安装使用
##### 安装
1.  **docker-ce环境**
2.  OpenVidu相关安装（docker一键安装，更多方式请看[文档](http://openvidu_cn.gitee.io/openvidu_docs_cn/docs/deployment)）

    * 下面的127.0.0.1这个ip改成自己的局域网ip，这样可以在局域网里使用( **注意Windows下是固定的IP:192.168.99.100** );

```
docker run -p 4443:4443 --rm \
	-e openvidu.secret=wesleyone \
	-e openvidu.publicurl=https://127.0.0.1:4443/ \
    -v /var/run/docker.sock:/var/run/docker.sock \
openvidu/openvidu-server-kms:2.12.0
```
docker镜像启动成功后，进行如下测试：
* 浏览器打开 `https://127.0.0.1:4443`,并允许HTTPS访问
* 输入帐号`OPENVIDUAPP`和密码`wesleyone`
* 点击TEST按钮，再次输入密码`wesleyone`，出现如下对勾画面则表示安装成功

![TEST成功](https://images.gitee.com/uploads/images/2020/0404/190555_09b595e2_1581722.jpeg "test_suc.jpg")

##### 使用
**docker运行shixun**
* 下面的127.0.0.1这个ip改成自己的局域网ip，这样可以在局域网里使用( **注意Windows下是固定的IP:192.168.99.100** );
```
docker run -d -p 8899:8899 --name=shixun \
    -e openvidu.secret=wesleyone \
    -e openvidu.publicurl=https://127.0.0.1:4443/ \
    -v /var/run/docker.sock:/var/run/docker.sock \
registry.cn-shanghai.aliyuncs.com/openvidu_cn/shixun:2.12.0
```

或者**idea上执行** 
环境要求至少**jdk8**,**maven3.5***
1. 将项目导入开发工具，推荐使用IDEA
2. 修改配置文件application.properties中的openvidu.url为自己的局域网ip(如果前面docker部署openvidu时修改了的话)
3. 执行com.easy.lab.shixun.ShixunApplication的main方法
4. 打开浏览器输入 https://127.0.0.1:8899(其他局域网内的设备可以访问https://内网ip:8899)
（成员帐号密码可以到db/data.sql中查看，当前使用H2数据库的内存形式）

**常见问题**
* 访问8899页面前必须要先访问4443并TEST成功
* https://127.0.0.1:8899需要允许https访问
* 执行restart.sh没有权限,请执行 chmod 755 restart.sh
* [查看更多问题](http://openvidu_cn.gitee.io/openvidu_docs_cn/docs/troubleshooting)

#### 云上部署
[阿里云ECS部署OpenVidu+KMS+COTURN，一篇文章就够了，更有官方授权的官网DOCS文档翻译](http://openvidu_cn.gitee.io/openvidu_docs_cn/docs/deployment/deploying-aliyun/)
[![阿里云推广](https://images.gitee.com/uploads/images/2020/0329/173435_131b3034_1581722.jpeg)](https://www.aliyun.com/minisite/goods?userCode=dnuqwh0e)


#### 鸣谢
<ul>
    <li><a href="https://openvidu.io/">OpenVidu</a></li>
    <li><a href="https://promotion.aliyun.com/ntms/yunparter/invite.html?userCode=dnuqwh0e" target="_blank">阿里云推广计划</a></li>
</ul>

#### 联系
钉钉群号：21919158

![扫码加入钉钉群](https://images.gitee.com/uploads/images/2020/0208/130429_13434bda_1581722.png "openvidu钉钉群小.png")，